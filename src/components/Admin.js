import React, { Component } from 'react';
import '../utils/Style.css';
import {getBicis} from '../utils/Controller';
import Mapa from './Mapa';
import PageTitle from '../utils/PageTitle';

export default class Admin extends Component {
    constructor(props){
        super(props);
        this.state={
            bicis:[],
        };
    }
    startOver(){
        let self = this;
        getBicis().then(value=>{
            let bicis = value.data;
            console.log('value: ', value);
            self.setState({bicis})
        }).catch(error=>{
            console.log('error: ', error);
            alert('Error al obtener datos');
        })
    }
    componentWillMount(){
        this.startOver();
    }
    render() {
        return(
            <div className="App">
                <PageTitle 
                    title="Bici-Q"
                    isLoggedIn={false}
                    hasSettingsMenu ={false}
                />
                <Mapa bicis={this.state.bicis}></Mapa>
            </div>
        )
    }
}


