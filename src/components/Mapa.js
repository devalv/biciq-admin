/* eslint-disable no-undef */

import React, { Component } from 'react';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import '../utils/Style.css';
import CircularProgress from 'material-ui/CircularProgress';
import {Table,TableBody,TableHeader,TableHeaderColumn,TableRow,TableRowColumn} from 'material-ui/Table';
import IconButton from 'material-ui/IconButton';
import {DirectionsRenderer, Marker as MarkerGoogle } from "react-google-maps";
import {GoogleMapsWrapper} from '../utils/GoogleMapsWrapper';
import MarkerClusterer from "react-google-maps/lib/components/addons/MarkerClusterer";
import MarkerWithLabel from "react-google-maps/lib/components/addons/MarkerWithLabel";

export default class Mapa extends Component {
  constructor(props){
    super(props);
    this.state={
      isLoading:true,
      location: null,
      current: null,
      markers:[],
      directionsEnabled:false,
      directions:null
    };
  }
  getLocation(){
    if (navigator.geolocation) {
      let current = navigator.geolocation.getCurrentPosition(this.showPosition);
      this.setState({isLoading:false})
    } else { 
      alert( "Geolocalización falló");
      this.setState({isLoading:false})
    } 
  }
  handleLocation = (loc) =>{
    let loca = loc.split(',');
    let location = {
      lat:parseFloat(loca[0]),
      lng:parseFloat(loca[1])
    };
    this.setState({location})
    this.handleDirections(location);
  }
  handleDirections = (location) =>{
    let self = this;
    const DirectionsService = new google.maps.DirectionsService();
    let directions = null;
    let testing = DirectionsService.route({
      origin: new google.maps.LatLng(this.state.current.lat,this.state.current.lng),
      destination: new google.maps.LatLng(location.lat,location.lng),
      travelMode: google.maps.TravelMode.DRIVING,
    }, (result, status) => {
      if (status === google.maps.DirectionsStatus.OK) {
        directions = result;
      } else {
        console.error(`error al crear ruta ${result}`);
        alert('Error al crear ruta');
        return null;
      }
    });
    setTimeout(()=>{
      if(directions!==null){
        console.log('Calculando Ruta...');
        self.setState({directions,directionsEnabled:true})
      }
    },1000)
  }
  findMe = () =>{
    this.setState({location:this.state.current});
  }
  showPosition = (position) => {
    let location= {
      lat: position.coords.latitude,
      lng: position.coords.longitude
    }
    this.setState({location,current:location})
    console.log('location: ', location);
  }
  componentWillMount(){
    this.getLocation();
  }
  render() {
    let markers = this.props.bicis!==null &&
    Object.values(this.props.bicis).map((marker,index) => {
      return(
        <MarkerGoogle
        key={index}
        position={{ lat: parseFloat(marker.location.split(',')[0]), lng: parseFloat(marker.location.split(',')[1]) }}
        icon={{url:marker.photo,scaledSize:{width:40,height:30}, anchor: { x: 15, y: 15 }}}
        />
      );
    });
    if(this.state.location!==null && this.props.bicis!==null){
      let detalle= Object.values(this.props.bicis).map((element,index)=>{
        return(
          <TableRow key={index}>
            <TableRowColumn >
                <img src={element.photo} alt="Foto"  className="foto"/>
            </TableRowColumn>
            <TableRowColumn >
               {element.name}
            </TableRowColumn>
            <TableRowColumn >
              <TextField
                  hintText="Info"
                  fullWidth={true}
                  multiLine={true}
                  rows={2}
                  rowsMax={6}
                  value={element.info}
              />
            </TableRowColumn>
          </TableRow>
        )
      });
      return this.state.isLoading === true ? <div className="content"><CircularProgress color={blue500} size={100} thickness={7} /></div> : (
        <div>
          <GoogleMapsWrapper
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyCk-4kxTt01RhodOqnUCUK7PJYsQxQEOLM&v=3.exp&libraries=geometry,drawing,places"
            loadingElement={<div style={{ height: '100%' }} />}
            containerElement={<div style={{ height: '400px' }} />}
            mapElement={<div style={{ height: '100%' }} />}
            defaultZoom={18}
            center={this.state.location}>
            <MarkerClusterer
              averageCenter
              enableRetinaIcons
              gridSize={60}>
                <MarkerGoogle
                key = {'position'}
                position={this.state.current}
                icon={{url:require('../assets/pina.png'),scaledSize:{width:30,height:30}, anchor: { x: 15, y: 15 }}}
                />
                {markers}
            </MarkerClusterer>
            {
              this.state.directionsEnabled === true &&
              <DirectionsRenderer directions={this.state.directions}></DirectionsRenderer>
            }
          </GoogleMapsWrapper>
          <br />
          <div style={{padding:20}}>
              <Table responsive bodyStyle={{overflow:'visible'}}  selectable={false}>
                <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                  <TableRow>
                      <TableHeaderColumn >Foto</TableHeaderColumn>
                      <TableHeaderColumn >Nombre</TableHeaderColumn>
                      <TableHeaderColumn >Info</TableHeaderColumn>
                  </TableRow>
                </TableHeader>
                <TableBody displayRowCheckbox={false}>
                  {detalle}
                </TableBody>
              </Table>
          </div>
        </div>
      )
    } else return '';
  }
}


