import firebase from 'firebase'
const config = {
        apiKey: "AIzaSyASminrsP0A2OZznx6_D8Y4IapxQyPiFXE",
        authDomain: "bici-q.firebaseapp.com",
        databaseURL: "https://bici-q.firebaseio.com",
        projectId: "bici-q",
        storageBucket: "bici-q.appspot.com",
        messagingSenderId: "828610279423"
};
export const firebaseApp = firebase.initializeApp(config);
export const db = firebaseApp.database();
export const storage = firebaseApp.storage();
export const auth = firebaseApp.auth(); 
export const storageKey = 'KEY_FOR_LOCAL_STORAGE';
export const isAuthenticated = () => {
    return !!auth.currentUser || !!localStorage.getItem(storageKey);
}
export function logout(){
    return auth.signOut();
}