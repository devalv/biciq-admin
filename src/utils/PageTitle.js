import React, { Component } from 'react';
import AppBar from 'material-ui/AppBar';
import FlatButton  from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import {Link} from 'react-router-dom';
import Exit from 'material-ui/svg-icons/action/exit-to-app';
import Settings from 'material-ui/svg-icons/action/settings';
import {logout} from '../utils/Firebase';
import { blue500 } from '../../node_modules/material-ui/styles/colors';

export default class PageTitle extends Component {
    constructor(props){
        super(props);
        this.state={

        };
    }
    logOut =() =>{
      logout();
      console.log('here');
    }
    componentWillMount(){
        
    }
    render() {
        return (
            <AppBar 
            iconElementLeft={
                <Link to={"/"}><FontIcon style={{margin: '10px 0 0',color: '#000', verticalAlign:'middle'}} className="material-icons">home</FontIcon></Link>
            }
            title={this.props.title} 
            titleStyle={{color:'#000'}}
            style={{marginBottom:5,backgroundColor: 'transparent',marginTop:0,marginLeft:0,marginRight:0,width:'100%' }} 
            iconElementRight = {
              <div>
              {
                this.props.isLoggedIn===true?
                <div>
                  {
                    this.props.hasBackButton===true&&
                      <FontIcon style={{display: 'inline-block',color: '#000', verticalAlign:'middle'}} className="material-icons">arrow_back</FontIcon>
                  }
                  <FlatButton 
                    style={{display: 'inline-block',margin: '7px 0 0', color: '#000'}} 
                    onClick={this.logOut}
                    labelPosition="before"
                    icon = {<Exit />}
                  />
                </div>   
                :
                <div>
                {
                  this.props.hasSettingsMenu&&
                  <Link to={"/login"}>
                  <FlatButton 
                    style={{display: 'inline-block',margin: '7px 0 0', color: '#000'}} 
                    labelPosition="before"
                    icon = {<Settings />}
                  />
                  </Link>  
                }
                </div>
              }
              </div>
            } 
          />  
        );
    }
}


