import {auth,db, storage, logout} from './Firebase.js';
import axios from 'axios';
import {url} from './Config';

export function getBicis(){
    let options ={
        async: true,
        method: 'GET',
        url:url+'Bicis',
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
        },
    };
    return axios(options);
}